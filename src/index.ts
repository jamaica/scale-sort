import { parallelLimit } from 'async';
import * as debug from 'debug';

export interface InterfaceEntity {
    id: number;
}

export interface InterfaceRule<T extends InterfaceEntity, K extends InterfaceEntity> {
    calculate(data: T, entity: K): Promise<number>;
}

export interface InterfaceScaleSort<T extends InterfaceEntity, K extends InterfaceEntity> {
    sort(haystack: K[], data: T, options: InterfaceOptions): Promise<K[]>;

    attachRule(rule: InterfaceRule<T, K>): void;
}

export interface InterfaceOptions {
    parallelLimit?: number;
}

class ScaleSort<T extends InterfaceEntity, K extends InterfaceEntity> implements InterfaceScaleSort<T, K> {

    private rules: Array<InterfaceRule<T, K>> = [];

    public async sort(haystack: K[], data: T, options: InterfaceOptions = {}): Promise<K[]> {
        if (!data) {
            throw new Error(`Data required`);
        }
        if (!haystack || !haystack.length) {
            throw new Error(`Entity Collection is empty. Data: ${data}, Entities: ${haystack}`);
        }

        let weights = [];
        for (const entity of haystack) {
            const promise = new Promise((resolve) => {
                const tasks = this.rules.map((rule) => {
                    return async (callback) => {
                        const entityLog = debug(`app-${data.id}-${entity.id}-scale-sort`);
                        try {
                            const weight = await rule.calculate(data, entity);
                            entityLog('Rule %o applied, Weight: %o',
                                rule.constructor.name, weight,
                            );
                            callback(null, weight);
                        } catch (e) {
                            entityLog('%o rule failed. Exception %o', rule.constructor.name, e);
                            callback(e);
                        }
                    };
                });

                parallelLimit(tasks, options.parallelLimit || 5, (_, ruleCalculationResults) => {
                    const passedRules = ruleCalculationResults.filter((e) => typeof e !== 'undefined');
                    const weight = (passedRules.length) ? +passedRules.reduce(
                        (prev, curr) => (typeof curr !== 'undefined') ? prev + curr : prev, 0) / passedRules.length : 0;
                    resolve({ entity, weight: weight.toFixed(2) });
                });
            });
            weights.push(promise);
        }

        weights = await Promise.all(weights);
        weights.sort((a, b) => b.weight - a.weight);

        return weights.map((r) => r.entity);
    }

    public attachRule(rule: InterfaceRule<T, K>): void {
        this.rules.push(rule);
    }
}

export default ScaleSort;
