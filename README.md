### scale sort
Last update: 3/01/2018

#### Usage Example
const scaleSort = new ScaleSort();

const rule = new Rule();

scaleSort.attachRule(rule);

const matches = await scaleSort.sort({}, []);