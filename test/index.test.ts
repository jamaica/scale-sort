import ScaleSort, { InterfaceEntity, InterfaceRule, InterfaceScaleSort } from '../src';

export const RuleMock = {
    getInstance: (mockedMethods) => {
        const mock = jest.fn<InterfaceRule<InterfaceEntity, InterfaceEntity>>(() => mockedMethods);
        return new mock();
    },
};

beforeEach(() => {
    const data: InterfaceEntity = { id: 1 };
    const sut: InterfaceScaleSort<InterfaceEntity, InterfaceEntity> = new ScaleSort();
    const haystack: InterfaceEntity[] = [
        { id: 1 },
        { id: 2 },
        { id: 3 },
        { id: 4 },
        { id: 5 },
    ];

    this.data = data;
    this.haystack = haystack;
    this.sut = sut;

});

test('sort happy path', async () => {
    const rule = RuleMock.getInstance({
        calculate: jest.fn()
            .mockReturnValueOnce(Promise.resolve(1))
            .mockReturnValueOnce(Promise.resolve(2))
            .mockReturnValueOnce(Promise.resolve(3))
            .mockReturnValueOnce(Promise.resolve(4))
            .mockReturnValueOnce(Promise.resolve(5)),
    });
    this.sut.attachRule(rule);

    const result = await this.sut.sort(this.haystack, this.data);

    expect(result).toMatchSnapshot();
});

test('sort applies rules even if one failed ', async () => {
    const rule1 = RuleMock.getInstance({
        calculate: jest.fn()
            .mockReturnValueOnce(Promise.resolve(1))
            .mockReturnValueOnce(Promise.resolve(2))
            .mockReturnValueOnce(Promise.reject(new Error()))
            .mockReturnValueOnce(Promise.resolve(3))
            .mockReturnValueOnce(Promise.resolve(5.6)),
    });
    const rule2 = RuleMock.getInstance({
        calculate: jest.fn()
            .mockReturnValueOnce(Promise.resolve(3))
            .mockReturnValueOnce(Promise.resolve(1))
            .mockReturnValueOnce(Promise.resolve(5))
            .mockReturnValueOnce(Promise.resolve(5))
            .mockReturnValueOnce(Promise.reject(new Error())),
    });
    this.sut.attachRule(rule1);
    this.sut.attachRule(rule2);

    const result = await this.sut.sort(this.haystack, this.data);

    expect(result).toMatchSnapshot();
});

test('returns original dm on all rules failed ', async () => {
    const rule1 = RuleMock.getInstance({
        calculate: jest.fn()
            .mockReturnValueOnce(Promise.reject(new Error()))
            .mockReturnValueOnce(Promise.reject(new Error()))
            .mockReturnValueOnce(Promise.reject(new Error()))
            .mockReturnValueOnce(Promise.reject(new Error()))
            .mockReturnValueOnce(Promise.reject(new Error())),
    });
    const rule2 = RuleMock.getInstance({
        calculate: jest.fn()
            .mockReturnValueOnce(Promise.reject(new Error()))
            .mockReturnValueOnce(Promise.reject(new Error()))
            .mockReturnValueOnce(Promise.reject(new Error()))
            .mockReturnValueOnce(Promise.reject(new Error()))
            .mockReturnValueOnce(Promise.reject(new Error())),
    });
    this.sut.attachRule(rule1);
    this.sut.attachRule(rule2);

    const result = await this.sut.sort(this.haystack, this.data);

    expect(result).toMatchSnapshot();
});

test('sort throws error on empty dm', async () => {
    let err;
    try {
        await this.sut.sort([], this.data);
    } catch (e) {
        err = e;
    }

    expect(err).not.toBeUndefined();
});
