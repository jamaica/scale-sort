"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const async_1 = require("async");
const debug = require("debug");
class ScaleSort {
    constructor() {
        this.rules = [];
    }
    sort(order, haystack) {
        return __awaiter(this, void 0, void 0, function* () {
            const makeLog = debug(`app-${order.id}-decision-maker`);
            if (!haystack || !haystack.length) {
                throw new Error(`Entity Collection is empty. Order: ${order}, Entities: ${haystack}`);
            }
            let weights = [];
            for (const entity of haystack) {
                const promise = new Promise((resolve) => {
                    const tasks = this.rules.map((rule) => {
                        return (callback) => __awaiter(this, void 0, void 0, function* () {
                            const entityLog = debug(`app-${order.id}-${entity.id}-decision-maker`);
                            try {
                                const weight = yield rule.calculate(order, entity);
                                entityLog('Rule %o applied, Weight: %o', rule.constructor.name, weight);
                                callback(null, weight);
                            }
                            catch (e) {
                                entityLog('%o rule failed. Exception %o', rule.constructor.name, e);
                                callback(e);
                            }
                        });
                    });
                    async_1.parallelLimit(tasks, 5, (_, ruleCalculationResults) => {
                        const passedRules = ruleCalculationResults.filter(this.defined);
                        const weight = (passedRules.length) ? +passedRules.reduce(this.sum, 0) / passedRules.length : 0;
                        resolve({ entity, weight: weight.toFixed(2) });
                    });
                });
                weights.push(promise);
            }
            weights = yield Promise.all(weights);
            weights.sort((a, b) => b.weight - a.weight);
            /* TODO What happens if all rules fail for all drivers? */
            makeLog('Matched 3 ids: %o', weights);
            return weights.map((r) => r.entity);
        });
    }
    attachRule(rule) {
        this.rules.push(rule);
    }
    sum(prev, curr) {
        return (typeof curr !== 'undefined') ? prev + curr : prev;
    }
    defined(e) {
        return typeof e !== 'undefined';
    }
}
exports.default = ScaleSort;
//# sourceMappingURL=index.js.map