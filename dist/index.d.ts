export interface InterfaceEntity {
    id: number;
}
export interface InterfaceRule<T extends InterfaceEntity, K extends InterfaceEntity> {
    calculate(order: T, vehicle: K): Promise<number>;
}
export interface InterfaceScaleSort<T extends InterfaceEntity, K extends InterfaceEntity> {
    sort(order: T, haystack: K[]): Promise<K[]>;
    attachRule(rule: InterfaceRule<T, K>): void;
}
declare class ScaleSort<T extends InterfaceEntity, K extends InterfaceEntity> implements InterfaceScaleSort<T, K> {
    private rules;
    sort(order: T, haystack: K[]): Promise<K[]>;
    attachRule(rule: InterfaceRule<T, K>): void;
    private sum(prev, curr);
    private defined(e);
}
export default ScaleSort;
